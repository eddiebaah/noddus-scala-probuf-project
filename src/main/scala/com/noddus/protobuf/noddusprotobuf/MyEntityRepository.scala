package com.noddus.protobuf.noddusprotobuf

import java.util.UUID
import java.io.{BufferedOutputStream,FileOutputStream}
import scala.util.Random
import cats.effect._
import scala.collection.mutable.ListBuffer
import cats.FlatMap
import cats.implicits._
import cats.effect.IO
import entities._


final case class MyEntityRepository[F[_]]()(implicit e: Effect[F]) {

    def addMyEntity(myEntity: MyEntity): F[Unit] =
      e.delay {
        val ent = MyProtoEntity(id=myEntity.id, name=myEntity.name)
        val filename = "files/" + 
        Random.alphanumeric.take(10).mkString("")
        val bos = new BufferedOutputStream(new FileOutputStream(filename))
        bos.write(ent.toByteArray)
        bos.close()
      } 
      
      

}
object MyEntityRepository {
  def empty[F[_]](implicit m: Effect[F]): IO[MyEntityRepository[F]] = IO{new MyEntityRepository[F]()}
}