package com.noddus.protobuf.noddusprotobuf

import cats.effect.IO
import cats.Monad
import cats.FlatMap
import cats.implicits._
import cats.effect._
import fs2.StreamApp
import fs2.Stream
import io.circe.generic.auto._
import io.circe.syntax._

import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.dsl.io._

import scala.concurrent.ExecutionContext.Implicits.global

import entities._

object AppServer extends StreamApp[IO] with Http4sDsl[IO] {


  def service[F[_]](myentityRepo: MyEntityRepository[F])(implicit F: Effect[F]) = HttpService[F] {

    case req @ POST -> Root =>
         req.decodeJson[MyEntity]
           .flatMap(myentityRepo.addMyEntity)
           .flatMap(myentity => Response(status = Status.Created).withBody(myentity.asJson))

  }

  def stream(args: List[String], requestShutdown: IO[Unit]) =
    Stream.eval(MyEntityRepository.empty[IO]).flatMap { myentityRepo =>
    BlazeBuilder[IO]
      .bindHttp(8080, "0.0.0.0")
      .mountService(service(myentityRepo), "/")
      .serve
    }
}